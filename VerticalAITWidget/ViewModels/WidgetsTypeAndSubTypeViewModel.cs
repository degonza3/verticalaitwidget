﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using VerticalAITWidget.Models;

namespace VerticalAITWidget.ViewModels
{
    public class WidgetsTypeAndSubTypeViewModel
    {
        public  IEnumerable<WidgetType> WidgetTypes { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime CreatedDate { get; set; }
        public int WidgetTypeId { get; set; }

        public WidgetsTypeAndSubTypeViewModel()
        {
            Id = 0;
        }

        public WidgetsTypeAndSubTypeViewModel(Widget widget)
        {
            Id = widget.Id;
            Name = widget.Name;
            CreatedDate = widget.CreatedDate;
            WidgetTypeId = widget.WidgetTypeId;
        }
    }
}