namespace VerticalAITWidget.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncludeWidgetModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Widgets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        CreatedDate = c.DateTime(nullable: false),
                        WidgetTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WidgetTypes", t => t.WidgetTypeId, cascadeDelete: true)
                .Index(t => t.WidgetTypeId);
            
            CreateTable(
                "dbo.WidgetTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WidgetSubTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        WidgetTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WidgetTypes", t => t.WidgetTypeId, cascadeDelete: true)
                .Index(t => t.WidgetTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WidgetSubTypes", "WidgetTypeId", "dbo.WidgetTypes");
            DropForeignKey("dbo.Widgets", "WidgetTypeId", "dbo.WidgetTypes");
            DropIndex("dbo.WidgetSubTypes", new[] { "WidgetTypeId" });
            DropIndex("dbo.Widgets", new[] { "WidgetTypeId" });
            DropTable("dbo.WidgetSubTypes");
            DropTable("dbo.WidgetTypes");
            DropTable("dbo.Widgets");
        }
    }
}
