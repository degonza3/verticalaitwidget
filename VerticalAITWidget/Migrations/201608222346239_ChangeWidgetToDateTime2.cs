namespace VerticalAITWidget.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeWidgetToDateTime2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Widgets", "CreatedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Widgets", "CreatedDate", c => c.DateTime(nullable: false));
        }
    }
}
