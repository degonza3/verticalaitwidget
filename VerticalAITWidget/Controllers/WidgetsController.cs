﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VerticalAITWidget.Models;
using System.Data.Entity;
using VerticalAITWidget.ViewModels;

namespace VerticalAITWidget.Controllers
{
    public class WidgetsController : Controller
    {
        private ApplicationDbContext _context;

        public WidgetsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Widgets
        public ActionResult Index()
        {
            var widgets = _context.Widgets.Include(w => w.WidgetType).ToList();
            return View(widgets);
        }

        public ActionResult New()
        {
            var viewModel = new WidgetsTypeAndSubTypeViewModel
            {
                WidgetTypes = _context.WidgetTypes.ToList(),
                CreatedDate = DateTime.Now
            };

            return View("WidgetForm", viewModel);
        }

        [HttpPost]
        public ActionResult Save(Widget widget)
        {
            if(!ModelState.IsValid)
            {
                var viewModel = new WidgetsTypeAndSubTypeViewModel(widget)
                {
                    WidgetTypes = _context.WidgetTypes.ToList()
                };

                return View("WidgetForm", viewModel);
            }

            if(widget.Id == 0)
            {
                _context.Widgets.Add(widget);
            }

            else
            {
                var currentWidget = _context.Widgets.Single(w => w.Id == widget.Id);

                currentWidget.Name = widget.Name;
                currentWidget.CreatedDate = DateTime.Now;
                currentWidget.WidgetTypeId = widget.WidgetTypeId;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Widget");
        }
    }
}