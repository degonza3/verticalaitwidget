﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VerticalAITWidget.Startup))]
namespace VerticalAITWidget
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
