﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VerticalAITWidget.Models
{
    public class WidgetSubType
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public WidgetType WidgetType { get; set; }
        public int WidgetTypeId { get; set; }
    }
}